## api

### create student

POST 127.0.0.1:8080/student/register
```
{
	"firstName": "Test first name",
	"lastName": "test last name",
	"email": "test@123.com",
	"password": "test123",
	"indexNo": "123456",
	"faculty": "science",
	"contactNo": "77123456"
}
```


check email and send verification

### student signup verify
POST 127.0.0.1:8080/student/verify
```
{
	"email": "test1@123.com",
	"code": "613793"
}
```

### user auth
POST 127.0.0.1:8080/student/login
```
{
	"email": "test1@123.com",
	"password": "test123"
}
```

Response
```
{
    "token": "cc9642f4-c0e0-4d19-8c16-2c7223d50bd5"
}
```

### create admin

POST 127.0.0.1:8080/admin/register

```
{
	"firstName": "Admin first name",
	"lastName": "Admin last name",
	"email": "admin@123.com",
	"password": "admin123",
	"faculty": "science",
	"contactNo": "77123456"
}
```


check email and send verification

### student signup verify
POST 127.0.0.1:8080/admin/verify
```
{
	"email": "admin@123.com",
	"code": "151648"
}
```


### admin auth
POST 127.0.0.1:8080/admin/login
```
{
	"email": "admin@123.com",
	"password": "admin123"
}
```

Response
```
{
    "token": "cc9642f4-c0e0-4d19-8c16-2c7223d50bd5"
}
```

# Event Creation

## Get event types to show on drop down 
GET 127.0.0.1:8080/attribute/event_types
```
[
    {
        "id": 1,
        "name": "Event type 1"
    },
    {
        "id": 2,
        "name": "Event type 2"
    }
]
```


## Get Venues to show on drop down 
GET 127.0.0.1:8080/attribute/venues
```
[
    {
        "id": 1,
        "name": "Venue 1"
    },
    {
        "id": 2,
        "name": "Venue 2"
    }
]
```



