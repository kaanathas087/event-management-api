package com.moratuwa.events.services;

import com.moratuwa.events.models.EventType;
import com.moratuwa.events.models.Venue;
import com.moratuwa.events.repositories.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventService {

    @Autowired
    EventRepository eventRepository;


    // helpers attributes for event creation
    public List<EventType> getEventTypes(){
        return eventRepository.getEventTypes();
    }


    public List<Venue> getVenues(){
        return eventRepository.getVenues();
    }

}
