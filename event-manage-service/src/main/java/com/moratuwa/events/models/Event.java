package com.moratuwa.events.models;

import java.time.Instant;

public class Event {

    private long id;

    private String eventName;

    private Instant date;

    private long studentId;

    private Integer venueId;

    private Integer eventTypeId;

}
