package com.moratuwa.events.repositories;

import com.moratuwa.events.models.Token;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Slf4j
@Repository
public class TokenRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public void createToken(Token token, long userId, String role) {
        String sql = "INSERT INTO access_tokens (token, user_id, role) VALUES (?, ?, ?)";
        jdbcTemplate.update(sql, token.getToken(), userId, role);
        log.info("Token successfully created ");
    }

}
