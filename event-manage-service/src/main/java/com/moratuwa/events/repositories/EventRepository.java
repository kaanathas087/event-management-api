package com.moratuwa.events.repositories;

import com.moratuwa.events.models.Admin;
import com.moratuwa.events.models.EventType;
import com.moratuwa.events.models.Venue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class EventRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;




    // helpers attributes for event creation
    public List<EventType> getEventTypes(){
        String sql = "SELECT id, name FROM event_types";
        return jdbcTemplate.query(sql, new EventTypeMapper());
    }


    public List<Venue> getVenues(){
        String sql = "SELECT id, name FROM venues";
        return jdbcTemplate.query(sql, new VenueMapper());
    }



    private static final class EventTypeMapper implements RowMapper<EventType> {
        public EventType mapRow(ResultSet rs, int rowNum) throws SQLException {
            EventType eventType = new EventType();
            eventType.setId(rs.getInt("id"));
            eventType.setName(rs.getString("name"));
            return eventType;
        }
    }


    private static final class VenueMapper implements RowMapper<Venue> {
        public Venue mapRow(ResultSet rs, int rowNum) throws SQLException {
            Venue venue = new Venue();
            venue.setId(rs.getInt("id"));
            venue.setName(rs.getString("name"));
            return venue;
        }
    }

}
