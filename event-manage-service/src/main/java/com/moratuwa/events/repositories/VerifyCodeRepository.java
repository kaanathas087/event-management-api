package com.moratuwa.events.repositories;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Slf4j
@Repository
public class VerifyCodeRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;


    public void createCode(String email, String code, String userType){
        String sql = "INSERT INTO verification_code (email, code, user_type) VALUES (email, code, userType)";
        jdbcTemplate.update(sql, email,  code, userType);
        log.info("Verify Code create successfully");
    }

    public String getCode(String email, String userType){
        String sql = "SELECT code FROM verification_code WHERE email= email AND user_type = userType ORDER BY create_time DESC LIMIT 1";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[] { email, userType}, String.class);
        } catch (EmptyResultDataAccessException e) {
            log.error("No code found");
        }
        return null;
    }


}
