package com.moratuwa.events.controllers;

import com.moratuwa.events.dto.LoginRequest;
import com.moratuwa.events.dto.RegisterRequest;
import com.moratuwa.events.dto.Response;
import com.moratuwa.events.dto.VerifyRequest;
import com.moratuwa.events.models.Token;
import com.moratuwa.events.services.AdminService;
import com.moratuwa.events.services.VerifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    AdminService adminService;

    @Autowired
    VerifyService verifyService;

    @PostMapping(value = "/register")
    public ResponseEntity<?> registerAdmin(@RequestBody RegisterRequest registerRequest){
        boolean isCreated = adminService.createAdmin(registerRequest);

        if(!isCreated){
            return new ResponseEntity<>(new Response("Error creating admin please try again"), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new Response("Successfully Created, Please check your email for verification"), HttpStatus.OK);
    }


    @PostMapping(value = "/verify")
    public ResponseEntity<?> registerAdminVerification(@RequestBody VerifyRequest verifyRequest){

        boolean isActivated =  verifyService.verify(verifyRequest.getEmail(), verifyRequest.getCode(), "admin");

        if(!isActivated){
            return new ResponseEntity<>(new Response("Invalid Token or Token not found"), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>("Activation success", HttpStatus.OK);

    }


    @PostMapping(value = "/login")
    public ResponseEntity<?> adminLogin(@RequestBody LoginRequest loginRequest){


        Token token = adminService.adminLogin(loginRequest);

        if(token == null){
            return new ResponseEntity<>(new Response("Invalid Email or Password"), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(token, HttpStatus.OK);
    }

}
