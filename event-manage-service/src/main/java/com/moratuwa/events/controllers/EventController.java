package com.moratuwa.events.controllers;

import com.moratuwa.events.services.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/event")
public class EventController {

    @Autowired
    EventService eventService;

    @GetMapping(value = "/create")
    public ResponseEntity<?> eventCreate(){


        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

}
