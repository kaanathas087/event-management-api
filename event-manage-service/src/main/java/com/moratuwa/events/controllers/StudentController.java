package com.moratuwa.events.controllers;

import com.moratuwa.events.dto.LoginRequest;
import com.moratuwa.events.dto.RegisterRequest;
import com.moratuwa.events.dto.Response;
import com.moratuwa.events.dto.VerifyRequest;
import com.moratuwa.events.models.Token;
import com.moratuwa.events.services.StudentService;
import com.moratuwa.events.services.VerifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    StudentService studentService;

    @Autowired
    VerifyService verifyService;


    @PostMapping(value = "/register")
    public ResponseEntity<?> registerStudent(@RequestBody RegisterRequest registerRequest){

        boolean isCreated = studentService.createStudent(registerRequest);

        if(!isCreated){
            return new ResponseEntity<>(new Response("Error creating student please try again"), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new Response("Successfully Created, Please check your email for verification"), HttpStatus.OK);
    }

    @PostMapping(value = "/verify")
    public ResponseEntity<?> registerStudentVerification(@RequestBody VerifyRequest verifyRequest){

        boolean isActivated =  verifyService.verify(verifyRequest.getEmail(), verifyRequest.getCode(), "user");
        if(!isActivated){
            return new ResponseEntity<>(new Response("Invalid Token or Token not found"), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Activation success", HttpStatus.OK);
    }

    @PostMapping(value = "/login")
    public ResponseEntity<?> studentAuth(@RequestBody LoginRequest loginRequest){

        Token token = studentService.studentLogin(loginRequest);

        if(token == null){
            return new ResponseEntity<>(new Response("Invalid Email or Password"), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(token, HttpStatus.OK);
    }
}
